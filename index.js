
const FIRST_NAME = "Anda";
const LAST_NAME = "Netejoru";
const GRUPA = "1092";

/**
 * Make the implementation here
 */
function initCaching() {
    var cacheObj = {};
    var obj = {};
    cacheObj.pageAccessCounter = function (param) {
		var page = 'home';
		if(param!=null){
			page = String(param).toLowerCase();
		}
       
		if(obj.hasOwnProperty(page)){
			obj[page] ++; 
		}
		else {
			obj[page] = 1;
		}
        
    };
    cacheObj.getCache = function () {
        return obj;
    };
    return cacheObj;
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

